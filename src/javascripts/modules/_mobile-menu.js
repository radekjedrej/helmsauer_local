import $ from "jquery"

class MobileMenu {
  constructor() {
    this.burger = $(".l-navbar__burger")
    this.menu = $(".m-menu")
    this.link = $(".m-menu__link")
    this.dropdownLink = $(".m-menu__dropdown__link")

    this.events()
  }

  events() {
    this.burger.on("click", this.openMenu.bind(this))
    this.link.on("click", this.openDropdown)
    this.dropdownLink.on("click", this.openSubmenu)
  }

  openMenu(e) {
    this.menu.slideToggle()
  }

  openDropdown(e) {
    $(this).toggleClass("m-menu__link--active")
    const drop = $(this).siblings(".m-menu__dropdown")
    drop.slideToggle()
  }

  openSubmenu(e) {
    $(this).toggleClass("m-menu__dropdown__link--active")
    const sub = $(this).siblings(".m-menu__submenu")
    sub.slideToggle()
  }
}

export default MobileMenu
