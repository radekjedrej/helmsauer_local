import $ from "jquery"

class Submenu {
  constructor() {
    this.link = $(".l-navbar__link")
    this.dropdownItem = $(".l-navbar__dropdown__item")
    this.submenu = $(".l-navbar__submenu")
    this.events()
  }

  events() {
    this.link.on("click", this.openDropdown)
    this.dropdownItem.on("click", this.openSubmenu)
  }

  openDropdown(e) {
    const dropdown = $(this).parent().children(".l-navbar__dropdown")

    $(this).toggleClass("active")
    dropdown.toggleClass("open")
  }

  openSubmenu(e) {
    const siblings = $(this).siblings()

    siblings.each(function (index, el) {
      let subMenu = el.querySelector(".l-navbar__submenu")

      if (subMenu) {
        subMenu.classList.remove("open")
        $(this).removeClass("active")
      }
    })
    // siblings.children.removeClass("open")
    console.log(siblings)

    const submenu = $(this).children(".l-navbar__submenu")
    const dropdown = $(this).parent().parent()

    if (submenu.length > 0) {
      if ($(e.target).is(".l-navbar__dropdown__link")) {
        $(this).toggleClass("active")
        submenu.toggleClass("open")
        dropdown.toggleClass("active")
      }
    }
  }
}

export default Submenu
