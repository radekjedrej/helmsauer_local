import MobileMenu from "./modules/_mobile-menu"
import Submenu from "./modules/_submenu"

document.addEventListener("DOMContentLoaded", function () {
  new Submenu()
  new MobileMenu()
})

console.log("Hello from main page!")
