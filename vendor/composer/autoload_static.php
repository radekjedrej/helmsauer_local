<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInite52a117ef421bea4556a55c893039ada
{
    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->classMap = ComposerStaticInite52a117ef421bea4556a55c893039ada::$classMap;

        }, null, ClassLoader::class);
    }
}
